(ns clj-proxy.core
  (:use [clojure.core.async :only [chan go close! >! <! >!! <!!]]
        [org.httpkit.server :as server]
        [org.httpkit.client :as http]))

(def ^:private host-mapping
  {"test1.example:8080" "http://localhost:8000"
   "test2.example:8080" "http://localhost:8000"
   "test3.example:8080" "http://www.google.com"})

(defn- build-url [req host]
  (let [base (str (host-mapping host) (:uri req))
        query-string (:query-string req)]
    (if (empty? query-string)
      base
      (str base "?" query-string))))

(defn- xform-headers [req host]
  req)

(defn req-handler [req]
  (let [host ((:headers req) "host")]
    (println "Host:" host)
    (println req)
    (if (contains? host-mapping host)
      (let [url (build-url req host)
            _ (println url)
            {:keys [status headers body error] :as resp}
            @(http/get url (xform-headers req host))]
        {:status status :body body})
      {:status 404 :body (str "No host found for: " host)})
    )
  )

(defn -main []
  (server/run-server req-handler {:port 8080}))
